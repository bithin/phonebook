class DuplicateUserException(Exception):
    def __init__(self):
        self.msg = 'Error! User already exists! Try again!'

    def __str__(self):
        return self.msg


class DuplicateContactException(Exception):
    def __init__(self):
        self.msg = 'Error! Contact already exists! Try again!'

    def __str__(self):
        return self.msg
