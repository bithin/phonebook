import xml.etree.ElementTree as ET

from PhonebookExceptions import *


class PhonebookHelper(object):
    """
    Helper class with all utility functions
    """
    def __init__(self, filename):
        self.filename = filename
        self.tree = ET.parse(self.filename)
        self.root = self.tree.getroot()
        return

    def add_user(self, username, password):
        """
        Adds a new user to the phonebook
        """
        for user in self.root.findall('userdetails'):
            if user.find('username').text == username:
                raise DuplicateUserException

        elem_userdet = ET.SubElement(self.root, "userdetails")
        elem_username = ET.SubElement(elem_userdet, "username")
        elem_username.text = username
        elem_password = ET.SubElement(elem_userdet, "password")
        elem_password.text = password
        self.indent(elem_userdet)
        return

    def add_contact(self, owner, name, phone, email):
        """
        Add a new contact to the phonebook
        """
        for contact in self.root.findall('contact'):
            if contact.attrib['owner'] == owner and contact.find('phone').text == phone:
                raise DuplicateContactException

        elem_contact = ET.SubElement(self.root, "contact")
        elem_contact.attrib["owner"] = owner
        elem_name = ET.SubElement(elem_contact, "name")
        elem_name.text = name
        elem_phone = ET.SubElement(elem_contact, "phone")
        elem_phone.text = phone
        elem_email = ET.SubElement(elem_contact, "email")
        elem_email.text = email
        self.indent(elem_contact)
        return

    def dump_data(self):
        """
        Dump all XML data to the file
        """
        self.tree.write(self.filename)
        return

    def auth_user(self, username, password):
        """
        User authentication using XPath Query.
        """
        path = "./userdetails/[username='"+ username +"']"
        node = self.root.findall(path)
        if node:
            xml_password = node[0].findtext("./password", "")
            if xml_password == password:
                return True

        return False

    def search_contact(self, key, username):
        """
        Search the PhoneBook for a contact.
        """
        path = "./contact/[name='"+key+"']/[@owner='"+username+"']"
        nodes = self.root.findall(path)
        user_details = []
        if nodes:
            for node in nodes:
                c = {}
                c['name'] = node.find('name').text
                c['phone'] = node.find('phone').text
                c['email'] = node.find('email').text
                user_details.append(c)
        
        path = "./contact/[name='"+key+"']"
        nodes = self.root.findall(path)
        if nodes:
            for node in nodes:
                c = {}
                c['name'] = node.find('name').text
                c['phone'] = node.find('phone').text
                c['email'] = "Not Visible"
                user_details.append(c)

        return user_details

    def get_all_contacts(self):
        """
        Return a list of all contacts
        """
        contact_list = []
        for contact in self.root.findall('contact'):
            c = {}
            c['name'] = contact.find('name').text
            c['phone'] = contact.find('phone').text
            c['email'] = contact.find('email').text
            contact_list.append(c)

        return contact_list

    def get_user_contacts(self, owner):
        """
        Return a list of contacts of a particular user
        """
        contact_list = []
        for contact in self.root.findall('contact'):
            c = {}
            if contact.attrib['owner'] == owner:
                c['name'] = contact.find('name').text
                c['phone'] = contact.find('phone').text
                c['email'] = contact.find('email').text
                contact_list.append(c)

        return contact_list

    def indent(self, elem, level=0):
        """
        Pretty pretty function borrowed from
        http://effbot.org/zone/element-lib.htm#prettyprint
        """
        i = "\n" + level*"  "
        if len(elem):
            if not elem.text or not elem.text.strip():
                elem.text = i + "  "
            if not elem.tail or not elem.tail.strip():
                elem.tail = i
            for elem in elem:
                self.indent(elem, level+1)
            if not elem.tail or not elem.tail.strip():
                elem.tail = i
        else:
            if level and (not elem.tail or not elem.tail.strip()):
                elem.tail = i
        return
