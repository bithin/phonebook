PhoneBook is a web application which helps user to storage there contact details on-line.
User can create an account and add contact details like name, phone number and email ad-
dress. The details like name and phone number will be public and will be visible to all the
user registered of PhoneBook site. The whole application is build using python from scratch
and uses python-twisted to create web server.
