from twisted.web.server import Site
from twisted.web.resource import Resource
from twisted.internet import reactor
from twisted.web.util import redirectTo

from template import index
from PhonebookHelper import PhonebookHelper
from PhonebookExceptions import DuplicateUserException, \
                                DuplicateContactException

class LoginPage(Resource):

    sessions = {}
    def render_GET(self, request):
        session_id = request.getSession().uid
        if session_id not in self.sessions.keys():
            return index.front()

        return redirectTo("form", request)

    def render_POST(self, request):
        session_id = request.getSession().uid
        username = request.args['username'][0]
        password = request.args['password'][0]
        user = PhonebookHelper("store.xml")
        if user.auth_user(username, password):
            self.sessions[session_id] = username
            return redirectTo("form", request)


        return redirectTo("", request)

class RegisterPage(Resource):

    def __init__(self):
        self.session = LoginPage().sessions
        self.session_id = ""
        self.name = ""
        self.username = ""
        self.email = ""

    def render_GET(self, request):
        self.session_id = request.getSession().uid
        if self.session_id not in self.session.keys():
            return index.register()

        return redirectTo("", request)

    def render_POST(self, request):
        self.session_id = request.getSession().uid
        if self.session_id not in self.session.keys():
            self.username = request.args['username'][0]
            self.password = request.args['password'][0]

            phone = PhonebookHelper("store.xml")
            if self.username != " " and self.password != " ":
                try:
                    phone.add_user(self.username, self.password)
                    phone.dump_data()
                    return '<html><body>You have successfully register \
                        please <a href="/">login</a>'
                except DuplicateUserException as e:
                    return '<html><body>%s</body></html>'%e
            else:
                return redirectTo("register", request)

        return redirectTo("", request)


class AddContactPage(Resource):

    def __init__(self):
        self.session = LoginPage().sessions
        self.session_id = ""
        self.username = ""
        self.name = ""
        self.phone = 0
        self.email = ""

    def render_GET(self, request):
        session_id = request.getSession().uid
        if session_id not in self.session.keys():
            return '<html><body>You are not authorized to view this page</body></html>'

        return index.add_contact()

    def render_POST(self, request):

        session_id = request.getSession().uid
        if session_id not in self.session.keys():
            return redirectTo("", request)
        self.username = self.session[session_id]
        self.name = request.args['name'][0]
        self.phone = request.args['phone'][0]
        self.email = request.args['email'][0]
        phone = PhonebookHelper("store.xml")
        if self.name != "" and self.phone != "":
            try:
                phone.add_contact(self.username, self.name, self.phone, self.email)
                phone.dump_data()
                return '<html><body>Successfully added <a href="/form">Home</form></body></html>'
            except DuplicateContactException as e:
                return '<html><body>%s<br /><a href="/form"> \
                        Home</a></body></html>'%e
        else:
            return redirectTo("addcontact", request)


class SearchPage(Resource):

    def __init__(self):
        self.key = ""
        self.user = ""
        self.session = LoginPage().sessions


    def render_GET(self, request):
        session_id = request.getSession().uid
        if session_id not in self.session.keys():
            return '<html><body>You are not authorized  \
                    Please <a href="/">Login</a></body></html>'
        
        return index.search(self.session[session_id])

    def render_POST(self, request):
        session_id = request.getSession().uid
        if session_id not in self.session.keys():
            return '<html><body>You are not authorized</body></html>'

        self.key = request.args['key'][0]
        self.user = request.args['user'][0]
        return index.search_result(self.key, self.user)

class HomePage(Resource):

    """
    Home Page
    """

    def __init__(self):
        self.username = ""
        self.session_id = ""

    def render_GET(self, request):
        session_id = request.getSession().uid
        if session_id not in LoginPage().sessions.keys():
            return '<html><body>You are not authorized, <a href="/">Login<a></body></html>'

        self.username = LoginPage().sessions[session_id]
        return index.home_user(self.username)


class LogoutPage(Resource):
    def render_GET(self, request):
        session_id = request.getSession().uid
        session = LoginPage().sessions
        if session_id in session.keys():
            request.getSession().expire()
            return '<html><body>You can now logged out<a href="/">Login</a></body></html>'

        return redirectTo("", request)

root = Resource()
root.putChild("", LoginPage())
root.putChild("form", HomePage())
root.putChild("logout", LogoutPage())
root.putChild("search", SearchPage())
root.putChild("register", RegisterPage())
root.putChild("addcontact", AddContactPage())
factory = Site(root)
reactor.listenTCP(8880, factory)
reactor.run()
