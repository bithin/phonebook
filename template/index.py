from PhonebookHelper import PhonebookHelper
def front():
    """
    Login page template

    """

    front_page = '<html><body><h2>A complete contact storage </h2><br /> \
            <form method="POST" name="login"> \
            <label for="username">Username</label><input type="text" name="username" /> <br /> \
            <label for="password">Password</label><input type="password" name="password" /> <br />\
            <input type="submit" name="loginsubmit" /> \
            </form><br /> \
            <a href="/register">Register</a> \
            </body></html>'

    return front_page

def register():
    """
    Register page template

    """
    register_page = '<html><body><h2>Register to PhoneBook</h2> \
            <form method="POST" name="register"> \
            <label for="username">Username</label><input type="text" name="username" /><br /> \
            <label for="password">Password</label><input type="password" name="password" /><br /> \
            <label for="submitregister">Register<label><input type="submit" name="submitregister" /> \
            </form> \
            </body></html>'

    return register_page

def add_contact():
    """
    Form to add contact details to PhoneBook
    """
    contact_form = '<html><body> \
        <form method="POST" name="contact"> \
        <label for="Name">Name</label><input type="text" name="name" /><br /> \
        <label for="phone">Phone</label><input type="text" name="phone" /><br /> \
        <label for="email">Email</label><input type="text" name="email" /><br /> \
        <input type="submit" name="addcontact" /> \
        </form> \
        <a href="/">Home</a><br /><a href="/search">Search PhoneBook</a><br /><a href="/logout">Logout</a> \
        </body></html>'

    return contact_form

def search(user):
    """
    Search form to search phonebook
    """
    search_form = '<html><body> \
        <h2>Search PhoneBook</h2> \
        <form method="POST" name="search"> \
        <label for="key">Contact Name</label><input type="text" name="key" /><br /> \
        <input type="hidden" name="user" value="'+ user +'"/> <br /> \
        <input type="submit" name="searchsubmit" /> \
        </form> <br /> \
        <a href="/">Home</a><br /><a href="/search">Search PhoneBook</a><br /><a href="/logout">Logout</a> \
        </body></html>'

    return search_form

def search_result(key, user):
    """
    Render search results
    """
    phone = PhonebookHelper("store.xml")
    contact_details = phone.search_contact(key, user)

    contact_template = '<html><body><h2>Search results for %s </h2>'%key
    contact_template += '<table border=0.5><th>Name</th><th>Phone No.</th><th>Email</th>'
    if contact_details:
        for contact in contact_details:
            contact_template += '<tr><td>'+contact['name']+'</td>'
            contact_template += '<td>'+contact['phone']+'</td>'
            contact_template += '<td>'+contact['email']+'</td></tr>'
    else:
        contact_template += '<h5> No result found</h5>'

    contact_template += '<table><br /><a href="/">Home</a><br /><a href="/search">Search PhoneBook</a><br /> \
                            <a href="/logout">Logout</a></body></html>'
    return contact_template

def home_user(username):
    """
    Generate template for home page
    """

    phone = PhonebookHelper("store.xml")
    contact_list = phone.get_user_contacts(username)
    contact_template = '<table border=0.68><h2>Your Contacts</h2><th>Name</th><th>Phone</th><th>Email</th>'
    for contact in contact_list:
        contact_template += '<tr><td>'+contact['name']+'</td><td>'+contact['phone']+'</td><td>'+contact['email']+'</td></tr>'

    contact_template += '</table>'
    all_contact_list = phone.get_all_contacts()
    all_contact_template = '<table border=0.68><h2>All Contacts</h2><th>Name</th><th>Phone</th>'
    for contact in all_contact_list:
        all_contact_template += '<tr><td>'+contact['name']+'</td><td>'+contact['phone']+'</td>'

    all_contact_template += '<table>'
    head = '<html><body> \
            <h2> Welcome to PhoneBook </h2><br /> \
            '+contact_template+' <br /> \
            '+all_contact_template+'<a href="/addcontact">Add Contact</a><br /><a href="/">Home</a><br /> \
            <a href="/search">Search PhoneBook</a><br /><a href="/logout">Logout</a></body></html>'

    return head

